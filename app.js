const tooltips = document.querySelectorAll(".all-tooltip .tooltip");
const fullDiv = document.querySelector("section");
const container = document.querySelector(".container");
let timeoutId;
window.addEventListener("resize", contentPosition);
window.addEventListener("DOMContentLoaded", contentPosition);


function contentPosition() {
  tooltips.forEach((tooltip) => {
    const pin = tooltip.querySelector(".pin");
    const content = tooltip.querySelector(".tooltip-content");
    const arrow = tooltip.querySelector(".arrow");
    const pinLeft = pin.offsetLeft;
    if (pinLeft + content.offsetWidth / 2 > fullDiv.offsetWidth) {
      const extraLeft =
        fullDiv.offsetWidth - (pinLeft + content.offsetWidth / 2);
      // console.log('right-conflict', tooltip)
      content.style.left =
        pinLeft - content.offsetWidth / 2 + extraLeft - 30 + "px";
      content.style.top = pin.offsetTop + 30 + "px";
    } else if (
      pin.offsetLeft + container.offsetLeft <
      content.offsetWidth / 2
    ) {
      // console.log('left conflict', pin.offsetLeft)
      content.style.left = -container.offsetLeft + "px";
      content.style.top = pin.offsetTop + 30 + "px";
    } else {
      content.style.left = pinLeft - content.offsetWidth / 2 + "px";
      content.style.top = pin.offsetTop + 30 + "px";
    }
    arrow.style.left =
      pinLeft - content.offsetLeft + pin.offsetWidth / 2 + "px";
  });
}
tooltips.forEach((tooltip) => {
  const pin = tooltip.querySelector(".pin");
  const content = tooltip.querySelector(".tooltip-content");
  pin.addEventListener("mouseover", () => {
    tooltip.classList.add("active");
  });
  pin.addEventListener("mouseleave", () => {
    timeoutId = setTimeout(() => {
      if (!tooltip.classList.contains("content-hover")) {
        tooltip.classList.remove("active");
      }
    },500);
  });
  content.addEventListener("mouseover", () => {
    clearTimeout(timeoutId);
    tooltip.classList.add("active");
    tooltip.classList.add("content-hover");
  });
  content.addEventListener("mouseleave", () => {
    timeoutId = setTimeout(() => {
      tooltip.classList.remove("active");
      tooltip.classList.remove("content-hover");
    }, 500);
  });
});

function fadeOut(el) {
    el.style.opacity = 1;
    (function fade() {
      if ((el.style.opacity -= .1) < 0) {
          el.style.display = "none";
      } else {
          requestAnimationFrame(fade);
      }
    })();
    };
    
    function fadeIn(el, display) {
    el.style.opacity = 0;
    el.style.display = display || "block";
    (function fade() {
      var val = parseFloat(el.style.opacity);
      if (!((val += .1) > 1)) {
          el.style.opacity = val;
          requestAnimationFrame(fade);
      }
    })();
    };

    //side navbar

const sidebarWrapper = document.getElementById('sidebar-wrapper');
let scrollToTopVisible = false;
// Closes the sidebar menu
const menuToggle = document.body.querySelector('.menu-toggle');
menuToggle.addEventListener('click', event => {
    event.preventDefault();
    sidebarWrapper.classList.toggle('active');
    _toggleMenuIcon();
    menuToggle.classList.toggle('active');
})

// Closes responsive menu when a scroll trigger link is clicked
var scrollTriggerList = [].slice.call(document.querySelectorAll('#sidebar-wrapper .js-scroll-trigger'));
scrollTriggerList.map(scrollTrigger => {
    scrollTrigger.addEventListener('click', () => {
        sidebarWrapper.classList.remove('active');
        menuToggle.classList.remove('active');
        _toggleMenuIcon();
    })
});

function _toggleMenuIcon() {
    const menuToggleBars = document.body.querySelector('.menu-toggle > .fa-bars');
    const menuToggleTimes = document.body.querySelector('.menu-toggle > .fa-xmark');
    if (menuToggleBars) {
        menuToggleBars.classList.remove('fa-bars');
        menuToggleBars.classList.add('fa-xmark');
    }
    if (menuToggleTimes) {
        menuToggleTimes.classList.remove('fa-xmark');
        menuToggleTimes.classList.add('fa-bars');
    }
}

document.addEventListener('DOMContentLoaded', function () {
  const accordions = document.querySelectorAll('.accordion-item');

  accordions.forEach(accordion => {
    const collapseElement = accordion.querySelector('.accordion-collapse');

    collapseElement.addEventListener('show.bs.collapse', function () {
      // Scroll to the accordion section when it is about to be shown
      accordion.scrollIntoView({ behavior: 'smooth', block: 'start' });
    });
  });
});


document.addEventListener("DOMContentLoaded", function() {
  const prevPageBtn = document.querySelector(".prev-page");
  const nextPageBtn = document.querySelector(".next-page");
  const targetElement = document.getElementById("target-element");

  const pageURLs = [
    "artworks.html",
    "artworks2.html",
  ];

  let currentPage = 0;

  function loadPage(pageIndex) {
    fetch(pageURLs[pageIndex])
      .then(response => response.text())
      .then(html => {
        targetElement.innerHTML = html;
      })
      .catch(error => {
        console.error("Error loading content:", error);
      });
  }

  prevPageBtn.addEventListener("click", () => {
    if (currentPage > 0) {
      currentPage--;
      loadPage(currentPage);
    }
  });

  nextPageBtn.addEventListener("click", () => {
    if (currentPage < pageURLs.length - 1) {
      currentPage++;
      loadPage(currentPage);
    }
  });

  loadPage(currentPage);
});


